cmake_minimum_required(VERSION 3.1.0)

project(qthf)

find_package(Qt5 COMPONENTS Core REQUIRED)

add_executable(qthf
    main.cpp
)

target_link_libraries(qthf Qt5::Core)