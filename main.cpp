#include <iostream>
#include <Qt>
#include <QDirIterator>
#include <QFileInfo>
#include <QDateTime>

int argcheck(int argc, char **argv, int &amount)
{
    int errorcode = 0;
    char *endptr;

    if (argc < 2)
    {
        errorcode = 1;
    }
    else if (argc < 3)
    {
        errorcode = 2;
    }  
    else if (argc > 3)
    {
        errorcode = 6;
    }
    else if (amount = std::strtol(argv[2], &endptr, 10))
    {
        if (amount < 0)
        {
            errorcode = 4;
        }
        if ((*endptr) != '\0')
        {
            errorcode = 5;
        }
    }
    else
    {
        errorcode = 3;
        if ((*endptr) != '\0')
        {
            errorcode = 5;
        }        
    }

    switch(errorcode)
    {
        case 1:
            std::cout << "No path given!" << std::endl;
            break;
        case 2:
            std::cout << "No amount parameter given!" << std::endl;
            break;
        case 3:
            std::cout << "The amount parameter is 0! -> " << argv[2] << std::endl;
            break;
        case 4:
            std::cout << "The amount parameter could not be a negative number!" << std::endl;
            break;        
        case 5:
            std::cout << "The amount parameter is not a valid integer number! -> " << argv[2]  << std::endl;
            break;    
        case 6:
            std::cout << "Too many arguments given!"<< std::endl;
            break;                                    
    }

    if (errorcode) 
    {
        std::cout << "usage: qthf [foldername] [amount (positive integer)]" << std::endl;
    }

    return errorcode;
}

int main(int argc, char **argv)
{
    int amount;

    if (int error = argcheck(argc, argv, amount))
    {
        return error;
    }

    int i = 0;

    QDirIterator dirIterator(
        argv[1],
        QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot,
        QDirIterator::Subdirectories);    

    while(dirIterator.hasNext() && (i < amount))
    {
        dirIterator.next();

        QFileInfo fileInfo = dirIterator.fileInfo();

        if (fileInfo.isDir())
        {
            QString dir = QString("Directory: %1").arg(fileInfo.absolutePath());
            std::cout << dir.toStdString() << std::endl;
        }
        if (fileInfo.isFile())
        {    
            QString fileDateTime = fileInfo.created().toString("yyyy-MM-dd HH:mm").leftJustified(30);
            QString fileSize = QString("%1 kb").arg(fileInfo.size()/1024).leftJustified(10);   
            QString file = QString("\t%1%2%3").arg(fileSize).arg(fileDateTime).arg(fileInfo.fileName());

            std::cout << file.toStdString() << std::endl;
        }

        i++;
    }

    return 0;
}
